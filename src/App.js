import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './index.css';

import { Grid, Row, Col, Table, Button, Glyphicon } from 'react-bootstrap';

export default class App extends Component {
    render() {
        return (
            <div>
                <div>
                    <Link to="/">Lista</Link>
                    <Link to="/adicionar">Adicionar</Link>
                </div>

                <Grid>
                    <Row>
                        <Col md={12}>
                            <h1>Rampas de cores</h1>
                        </Col>
                    </Row>

                    <Table striped bordered condensed>
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Pré-Visualização</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>5 Faixas - Menor</td>
                                <td>
                                    <div className="preview"></div>
                                </td>
                                <td>
                                    <Button>
                                        <Glyphicon glyph="edit" />
                                    </Button>
                                </td>
                                <td>
                                    <Button>
                                        <Glyphicon glyph="trash" />
                                    </Button>
                                </td>
                            </tr>
                            <tr>

                            </tr>
                        </tbody>
                    </Table>
                </Grid>
            </div>
        );
    }
}