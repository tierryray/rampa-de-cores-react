import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Preview from './Preview';
import '../css/adicionar.css';

import { GithubPicker } from 'react-color';

class Nome extends Component {
    render() {
        return (
            <div>
                <span>Nome</span>
                <br />
                <input type="text" />
            </div>
        );
    }
}

class Cores extends Component {

    constructor(props) {
        super(props);
        this.state = {
            color: this.props.color,
            displayColorPicker: this.props.displayColorPicker
        }
    }

    handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
    };

    handleChange = (color) => {
        this.setState({
            color: color.hex,
            displayColorPicker: false
        })
    };

    handleClose = () => {
        this.setState({ displayColorPicker: false })
    };

    componentWillReceiveProps(nextProps) {
        console.log("Fui chamado");
    }

    render() {
        
        const cardStyle = {
            background: `${this.state.color}`
        }

        return (
            <div>
                <div className="card" style={cardStyle} onClick={this.handleClick} />
                {
                    this.state.displayColorPicker
                        ? <div>
                            <div className="picker" />
                            <GithubPicker color={this.props.color} onChange={this.handleChange} />
                        </div>
                        : null
                }
            </div>
        );
    }
}


export default class Adicionar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            displayColorPicker: false,
            color: []
        };
    }

    render() {
        return (
            <div>
                <div>
                    <Link to="/">Lista</Link>
                    <Link to="/adicionar">Adicionar</Link>
                </div>

                <h1>Adicionar Rampa de Cores</h1>

                <form>
                    <Nome />
                    <Preview color={this.props.color}/>
                    <br />

                    <div>
                        <span>Cores</span>
                        <br />
                        <Cores color={this.props.color} displayColorPicker={this.props.displayColorPicker} />
                    </div>

                </form>


            </div>
        );
    }
}