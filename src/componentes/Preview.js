import React, { Component } from 'react';

export default class Preview extends Component {

    constructor(props) {
        super(props);
        this.state = {
            color: this.props.color
        }
    }

    render() {
        
        const style = {
            background: `${this.state.color}`
        }

        return (
            <div>
                <div>
                    <span>Pré-visualização</span>
                    <br />
                    <div className="preview" style={style} />
                </div>
            </div>
        );
    }
}